import React, { Component } from 'react';
import {Image, StyleSheet} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Left, Right, Body, Title, Card, CardItem, Thumbnail } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class Home extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.bgColor}>
          <Left>
            <Button transparent>
              <Icon type="AntDesign" name="home" />
            </Button>
          </Left>
          <Body>
            <Title style={{fontFamily:"vinchand", fontSize:30}}>Home</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card style={styles.cardRadius}>
            <CardItem style={styles.cardItemTop}>
              <Left>
                <Thumbnail source={require('../assets/images/avatar.png')} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://images.unsplash.com/photo-1601173662818-95e6e19c4c50?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1490&q=80'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem style={styles.cardItemBottom}>
              <Left>
                <Button transparent>
                  <Icon active type="FontAwesome" name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active type="FontAwesome" name="wechat" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>

          <Card style={styles.cardRadius}>
            <CardItem style={styles.cardItemTop}>
              <Left>
                <Thumbnail source={require('../assets/images/avatar.png')} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://images.unsplash.com/photo-1601173662818-95e6e19c4c50?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1490&q=80'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem style={styles.cardItemBottom}>
              <Left>
                <Button transparent>
                  <Icon active type="FontAwesome" name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active type="FontAwesome" name="wechat" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
          
        </Content>
        <Footer>
          <FooterTab style={styles.bgColor}>
            <Button vertical>
              <Icon type="AntDesign" name="appstore-o" />
              <Text>Apps</Text>
            </Button>
            <Button vertical onPress={()=> Actions.gallery({kirimData:"ini data akan dikirim", kirimData2:"ini data 2"})}>
              <Icon type="Entypo" name="camera" />
              <Text>Camera</Text>
            </Button>
            <Button vertical>
              <Icon type="MaterialIcons" name="person" />
              <Text>Person</Text>
            </Button>
            <Button vertical>
              <Icon type="AntDesign" name="contacts" />
              <Text>Contact</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  bgColor:{
    backgroundColor:"#f9690e",
  },
  cardRadius:{
    borderRadius:15,
  },
  cardItemTop:{
    borderTopLeftRadius:15,
    borderTopRightRadius:15,
  },
  cardItemBottom:{
    borderBottomLeftRadius:15,
    borderBottomRightRadius:15,
  }
});
import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Left, Right, Body, Title, Card, CardItem, Thumbnail } from 'native-base';
import { Actions } from 'react-native-router-flux';
export default class Gallery extends Component {
    constructor(props){
        super(props);
        this.state={
            terimaData:this.props.kirimData,
            terimaData2: this.props.kirimData2,
        }
    }
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon type="AntDesign" name="home" />
            </Button>
          </Left>
          <Body>
            <Title>Gallery</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
            <Text>{this.state.terimaData} - {this.state.terimaData2}</Text>
        </Content>
        <Footer>
          <FooterTab>
            <Button vertical onPress={()=> Actions.home()}>
              <Icon type="AntDesign" name="appstore-o" />
              <Text>Apps</Text>
            </Button>
            <Button vertical>
              <Icon type="Entypo" name="camera" />
              <Text>Camera</Text>
            </Button>
            <Button vertical active>
              <Icon active type="MaterialIcons" name="person" />
              <Text>Person</Text>
            </Button>
            <Button vertical>
              <Icon type="AntDesign" name="contacts" />
              <Text>Contact</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
import React, {Component} from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Gallery from './src/pages/Gallery';
import Home from './src/pages/Home';

export class Routes extends Component {
    render(){
        return(
            <Router>
                <Scene key="root" hideNavBar={true}>
                    <Scene key="home" component={Home} initial={true}/>
                    <Scene key="gallery" component={Gallery} />
                </Scene>
            </Router>
        );
    }
}